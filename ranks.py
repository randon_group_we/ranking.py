# file reading

COMMA = ","
data = []
def parse_line(line:str) -> list :
	name, *marks = line.strip().split(COMMA)
	marks = [int(_) for _ in marks]
	return [name] + marks 
def load_data(mark_file: str) -> list:
	return [parse_line(line) for line in open(mark_file)]

print(load_data("marks.txt"))


# ----main funcs
def is_higher(self_marks: list[int], other_marks: list[int]) -> bool:
        return all(a > b for a, b in zip(self_marks, other_marks))

class student:
    def __init__(self, name, marks : list[int]):
        self.name = name
        self.marks = marks

    def __repr__(self) -> str:
        return f'{self.name} marks: {self.marks}'
    
    def rank(self, other) -> str:
        return f'{self.name} > {other.name}' if is_higher(self.marks, other.marks) else f'{self.name}'




# data=
# A 12 14 16
# B 5 6  7
# C 17 20 23
# D 2 40 12
# E 3 41 13
# F 7 8 9
# G 4 5 6

