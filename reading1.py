SPACE = " "

COMMA = ","
data = []
def parse_line(line:str) -> list :
	name, *marks = line.strip().split(COMMA)
	marks = [int(_) for _ in marks]
	return [name] + marks 
def load_data(mark_file: str) -> list:
	return [parse_line(line) for line in open(mark_file)]


