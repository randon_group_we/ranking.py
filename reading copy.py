
# # SPACE = " "

# # COMMA = ","
# # data = []
# # def parse_line(line:str) -> list :
# # 	name, *marks = line.strip().split(COMMA)
# # 	marks = [int(_) for _ in marks]
# # 	return [name] + marks 
# # def load_data(mark_file: str) -> list:
# # 	return [parse_line(line) for line in open(mark_file)]

# # print(load_data("marks.txt"))


# # def compare(studentA: list[int], studentB: list[int]):
# #     for marks_A, marks_B in zip(studentA[1:], studentB[1:]):
# #         if marks_A <= marks_B:
# #             return False
# #     return True

# # def getRank(students: list[int]):
# #     n = len(students)
# #     higherPos = [[0] * n for _ in range(n)]

# #     for i in range(n):
# #         for j in range(n):
# #             if i != j and compare(students[i], students[j]):
# #                 higherPos[i][j] = 1

# #     def isHigher(i: int, ranked: list[int]):
# #         for j in range(n):
# #             if j not in ranked and higherPos[j][i] == 1:
# #                 return True
# #         return False

# #     ranking = []
# #     while len(ranking) < n:
# #         for i in range(n):
# #             if i not in ranking and not isHigher(i, ranking):
# #                 ranking.append(i)
# #                 break

# #     final_ranking = [students[i][0] for i in ranking]
    
# #     incomparableMarks = []
# #     for i in range(n):
# #         if i not in ranking:
# #             incomparableMarks.append(students[i][0])
    
# #     return final_ranking, incomparableMarks

# # students = [
# #     ["A", 12, 14, 16],
# #     ["B", 5, 6, 7],
# #     ["C", 17, 20, 23],
# #     ["D", 2, 40, 12],
# #     ["E", 3, 41, 13],
# #     ["F", 7, 8, 9],
# #     ["G", 4, 5, 6]
# # ]

# # rankList, case_2 = getRank(students)
# # print("Ranking:", rankList)
# # print("others:", case_2)
SPACE = " "
COMMA = ","
data = []

def parse_line(line: str) -> list:
    name, *marks = line.strip().split(COMMA)
    marks = [int(_) for _ in marks]
    return [name] + marks

def load_data(mark_file: str) -> list:
    with open(mark_file, 'r') as file:
        return [parse_line(line) for line in file]

def compare(studentA: list[int], studentB: list[int]):
    for marks_A, marks_B in zip(studentA[1:], studentB[1:]):
        if marks_A <= marks_B:
            return False
    return True

def getRank(students: list[list[int]]):
    n = len(students)
    higherPos = [[0] * n for _ in range(n)]

    for i in range(n):
        for j in range(n):
            if i != j and compare(students[i], students[j]):
                higherPos[i][j] = 1

    def isHigher(i: int, ranked: list[int]):
        for j in range(n):
            if j not in ranked and higherPos[j][i] == 1:
                return True
        return False

    ranking = []
    incomparableMarks = []
    while len(ranking) + len(incomparableMarks) < n:
        added = False
        for i in range(n):
            if i not in ranking and i not in incomparableMarks and not isHigher(i, ranking):
                ranking.append(i)
                added = True
                break
        if not added:
            for i in range(n):
                if i not in ranking and i not in incomparableMarks:
                    incomparableMarks.append(i)
                    added = True
                    break

    final_ranking = [students[i][0] for i in ranking]
    incomparable_students = [students[i][0] for i in incomparableMarks]
    
    return final_ranking, incomparable_students

students = load_data("studentRanking.txt")

rankList, case_2 = getRank(students)
print("Ranking:", rankList)
print("Others:", case_2)



