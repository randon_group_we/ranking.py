COMMA = ","

def parse_line(line: str) -> list:
    name, *marks = line.strip().split(COMMA)
    marks = [int(_) for _ in marks]
    return [name] + marks

def load_data(mark_file: str) -> list:
    return [parse_line(line) for line in open(mark_file)]

def is_higher(self_marks: list[int], other_marks: list[int]) -> bool:
    return all(a > b for a, b in zip(self_marks, other_marks))

class Student:
    def __init__(self, name, marks: list[int]):
        self.name = name
        self.marks = marks

    def __repr__(self) -> str:
        return f'{self.name} marks: {self.marks}'
    
    def rank(self, other) -> str:
        return f'{self.name} > {other.name}' if is_higher(self.marks, other.marks) else f'{self.name}'


def create_students(mark_file: str) -> list[Student]:
    data = load_data(mark_file)
    return [Student(name, marks) for name, *marks in data]


def rank_students(students: list[Student]) -> list[str]:
    rankings = []
    for i in range(len(students)):
        for j in range(i+1, len(students)):
            if is_higher(students[i].marks, students[j].marks):
                rankings.append(f'{students[i].name} > {students[j].name}')
            else:
                rankings.append(f'{students[j].name} > {students[i].name}')
    return rankings

students = create_students("marks.txt")

# Print the students
for student in students:
    print(student)

# Rank the students and print the rankings
rankings = rank_students(students)
for ranking in rankings:
    print(ranking)
