from reading1  import load_data

from itertools import permutations
data = load_data("marks1.txt")
def arrange(mark_list: list) -> list:
	def make_key(one: list) -> str:
		return one[1] and one[2] and one[3] 
	#return [name[0] for name in reversed(sorted(mark_list, key=make_key))]
	return [name for name in reversed(sorted(mark_list, key=make_key))]
print(arrange(data))
l = arrange(data)

def compare(x, y):
	return all([ i > j for i, j in zip(x[1:],y[1:])])



def relation(l: list) :
	s = ""
	for x, y in zip(l,l[1:]):
		if  compare(x, y):
			s += f'{x[0]} > {y[0]}, '
	return s

s =  ''
for x, y in permutations(l,2):
	if  compare(x, y):
		s += f'{x[0]} > {y[0]}, '

print(s)


print(relation(l))
	
	
